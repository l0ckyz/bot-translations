require('dotenv-flow').config();

module.exports = {
    /*
        English *Great Britain* Translation for the Dismon Discord Bot
    */
    
    //Global
    searchError: "This search has resulted in an error",

    //Info
    //Currently unavailable

    //ItemDex Command
    itemUnprovided: "An item needs to be entered",
    itemEffect: "Effect",
    itemMartCost: "Cost in Mart",
    itemMartUnpurchaseable: "Not Purchaseable",
    itemCategory: "Item Category",

    //Metrics Command
    cmdMetricsDisabled: "This command doesn\'t have metrics enabled.",
    metricsUses: "Uses",

    //PokeDex Command
    pokeUnprovided: "A pokemon needs to be entered",
    pokeTypes: "Types",
    pokeHeight: "Height",
    pokeWeight: "Weight",
    pokeExerienceBase: "Base Experience",
    pokeAbility: "Ability",
    pokeHiddenAbility: "Hidden Ability",

    //Settings Command
    //Currently unavailable

    //Typedex Command
    typeCMDUnprovided: "A type needs to be entered",
    typeCMDStrongAgainst: "Strong Against",
    typeCMDWeakAgainst: "Weak Against",

    //Types
    typeBug: "Bug",
    typeDark: "Dark",
    typeDragon: "Dragon",
    typeElectric: "Electric",
    typeFairy: "Fairy",
    typeFighting: "Fighting",
    typeFire: "Fire",
    typeFlying: "Flying",
    typeGhost: "Ghost",
    typeGrass: "Grass",
    typeGround: "Ground",
    typeIce: "Ice",
    typeNormal: "Normal",
    typePoison: "Poison",
    typePsychic: "Psychic",
    typeRock: "Rock",
    typeSteel: "Steel",
    typeWater: "Water"


    //NOTE, MANY strings are currently excluded. They will be added as we continue to work on translation integration.
}
