require('dotenv-flow').config();

module.exports = {
    /*
        Svensk översättning för JoiBoi Discord Botten
    */

    //Global
    mentionInvalid: "Vänligen ange en giltig medlem av denna server",
    noReason: "Ingen anledning försed",
    noUserPermission: "Du har INTE behörigheterna att använda detta kommando",
    commandFailDataCollect: "Kommandot kunde inte köras på grund av att datainsamling är inaktiverad.",
    reason: "Anledning",
    id: "ID",

    //Ban Command
    userUnbannable: "Jag kan inte banna denna medlem! Har de en högre roll? Har jag bannlysnings behörigheter?",
    banFail: "Ledsen jag kunde inte bannlysa:",
    banSuccess: " har bannlysts av **${message.author.username** för ",
    banEmbedAuthor: "Bannlys | ",
    banBy: "Bannlyst Av ",

    //Flip command
    coinHeads: "Krona",
    coinTails: "Klave",
    coinEmbedTitle: "Mynt Kast",
    coinFlipOutput: "Du fick en ",

    //Giveaway Commands have been skipped due to limitations
    //Help Command has been skipped due to future deprecation
    //Impostor Command
    impostorOutputImpostor: " var en Impostor",
    impostorOutputInnocent: " var inte en Impostor",

    //Info Command

    //Invite Command
    inviteCMDReply: "Jag har skickat inbjudan till dig i ett DM",
    inviteDM: "Din nya inbjudan är https://discord.gg/",

    //Kick Command
    userUnkickable: "Jag kan inte sparka denna användare! Har de en högre roll? Har jag sparknings behörigheter?",
    kickFail: "Ledsen ${message.author}, jag kunde inte sparka på grund av: ${error}",
    kickSuccess: " har sparkats av **${message.author.username}** för ",
    kickEmbedAuthor: "Sparka | ",
    kickBy: "Sparkad Av",

    //Markdown Command
    //Command is due to be rewritten so has been excluded from translation for now.

    //Metrics Command
    cmdMetricsDisabled: "Detta kommando har inte mätvärden aktiverade.",
    metricsUses: "Mängd andvändningar",

    //Mute Command
    //Currently unavailable

    //Points Command
    pointsTitle: "Poäng",
    pointsCurLevel: "Nuvarande Nivå",
    pointsCurPoints: "Nuvarande Poäng Totalt",
    pointsNeeded: "Poäng som behövs för nivå ${nextlevel} ${pointsNeed} poäng",

    //Purge Command
    //Currently unavailable

    //Dice Roll Command
    diceEmbedTitle: "Tärningskast",
    diceEmbedDescription: "Tärningen gav en ",

    //Server Info Command
    //Currently unavailable

    //Settings Command
    //Currently unavailable

    //top10 Command
    top10EmbedTitle: "Top Tio",
    top10EmbedDescription: "Våra Top 10 poängledare"

    //User Info Command
    //Currently unavailable

    //Warn Command
    //Currently unavailable

    //NOTE, MANY strings are currently excluded. They will be added as we continue to work on translation integration.
};
