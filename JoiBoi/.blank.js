require('dotenv-flow').config();

module.exports = {
    /*
        Template for Translation files for the Dismon Discord Bot
    */
    
    //Global
    mentionInvalid: "",
    noReason: "",
    noUserPermission: "",
    commandFailDataCollect: "",
    reason: "",
    id: "",

    //Ban Specific
    userUnbannable: "",
    banFail: "",
    banSuccess: "",
    banEmbedAuthor: "",
    banBy: "",

    //Flip command
    coinHeads: "",
    coinTails: "",
    coinEmbedTitle: "",
    coinFlipOutput: "",

    //Giveaway Commands have been skipped due to limitations
    //Help Command has been skipped due to future deprecation
    //Impostor Command
    impostorOutputImpostor: "",
    impostorOutputInnocent: "",

    //Info Command

    //Invite Command
    inviteCMDReply: "",
    inviteDM: "",

    //Kick Command
    userUnkickable: "",
    kickFail: "",
    kickSuccess: "",
    kickEmbedAuthor: "",
    kickBy: "",

    //Markdown Command
    //Command is due to be rewritten so has been excluded from translation for now.

    //Metrics Command
    cmdMetricsDisabled: "",
    metricsUses: "",

    //Mute Command
    //Currently unavailable

    //Points Command
    pointsTitle: "",
    pointsCurLevel: "",
    pointsCurPoints: "",
    pointsNeeded: "",

    //Purge Command
    //Currently unavailable

    //Dice Roll Command
    diceEmbedTitle: "",
    diceEmbedDescription: "",

    //Server Info Command
    //Currently unavailable

    //Settings Command
    //Currently unavailable

    //top10 Command
    top10EmbedTitle: "",
    top10EmbedDescription: ""

    //User Info Command
    //Currently unavailable

    //Warn Command
    //Currently unavailable

    //NOTE, MANY strings are currently excluded. They will be added as we continue to work on translation integration.
};
