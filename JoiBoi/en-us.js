require('dotenv-flow').config();

module.exports = {
    /*
        English *United States* Translation for the JoiBoi Discord Bot
    */

    //Global
    mentionInvalid: "Please mention a valid member of this server",
    noReason: "No reason provided",
    noUserPermission: "You do NOT have permission to use this command",
    commandFailDataCollect: "Command could not be executed due to data collection being disabled.",
    reason: "Reason",
    id: "ID",

    //Ban Specific
    userUnbannable: "I cannot ban this user! Do they have a higher role? Do I have ban permissions?",
    banFail: "Sorry I couldn't ban because of:",
    banSuccess: " has been banned by **${message.author.username}** for ",
    banEmbedAuthor: "Ban | ",
    banBy: "Banned By ",

    //Flip command
    coinHeads: "Heads",
    coinTails: "Tails",
    coinEmbedTitle: "Coin Flip",
    coinFlipOutput: 'You got a ',

    //Giveaway Commands have been skipped due to limitations
    //Help Command has been skipped due to future deprecation
    //Impostor Command
    impostorOutputImpostor: " was an Impostor",
    impostorOutputInnocent: " was not an Impostor",

    //Info Command
    //Currently unavailable

    //Invite Command
    inviteCMDReply: "I\'ve sent the invite to you in a DM.",
    inviteDM: "You\'re new invite is https://discord.gg/",

    //Kick Command
    userUnkickable: `I cannot kick this user! Do they have a higher role? Do I have kick permissions?`,
    kickFail: "Sorry ${message.author}, I couldn\'t kick because of: ${error}",
    kickSuccess: " has been kicked by **${message.author.username}** for ",
    kickEmbedAuthor: "Kick | ",
    kickBy: "Kicked By",

    //Markdown Command
    //Command is due to be rewritten so has been excluded from translation for now.

    //Metrics Command
    cmdMetricsDisabled: "This command doesn\'t have metrics enabled.",
    metricsUses: "Uses",

    //Mute Command
    //Currently unavailable

    //Points Command
    pointsTitle: "Points",
    pointsCurLevel: "Current Level",
    pointsCurPoints: "Current Point Total",
    pointsNeeded: "Points needed for level ${nextlevel} ${pointsNeed} points",

    //Purge Command
    //Currently unavailable

    //Dice Roll Command
    diceEmbedTitle: "Dice Roll",
    diceEmbedDescription: "The dice rolled ",

    //Server Info Command
    //Currently unavailable

    //Settings Command
    //Currently unavailable

    //top10 Command
    top10EmbedTitle: "Top Ten",
    top10EmbedDescription: "Our top 10 points leaders"

    //User Info Command
    //Currently unavailable

    //Warn Command
    //Currently unavailable

    //NOTE, MANY strings are currently excluded. They will be added as we continue to work on translation integration.
};
